<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <style type="text/css">
            table{border:1px solid black;}
            td{border:1px solid black;}
        </style>
    </head>
    <body>
        <?php
        
        /**
         * This is a test for JaitecAlias class that demostrates how large
         * can be the id to encode, and how the processes of encode-decode are
         * simetrics
         */
            require 'jaitec/alias/JaitecAlias.php';
            
            $alias = new JaitecAlias();
            
            ?>
            Calculating max values with n digits<br/>
            <table><tr><td># digits</td><td>Maximum alias</td><td>Maximum id</td></tr>
            <?php 
            // to know accuracy of conversion with actual base-array
            for ($n = 0; $n<8; $n++){
                $maxAlias = $alias->maxAlias($n);
                $maxId    = $alias->maxId($n);
            
                print "<tr><td>$n</td><td>$maxAlias</td><td>$maxId</td></tr>";
            }
            
            ?>
            </table>
            Now generating random values to test if encode-decode runs correctly<br/>
            <table><tr><td>#</td><td>Random value</td><td>Encode</td><td>Decode</td><td>Ok?</td></tr>
            <?php
            $n = 6;
            // calculates some random values to check if encode-decode runs ok
            for ($i = 0; $i<10; $i++){
                
                $id  = rand(0,$alias->maxId($n));
                $a   = $alias->encode($id, $n);
                $_id = $alias->decode($a);
                $eq  = ($id == $_id)?'OK':'Error';
                print("<tr><td>$i</td><td>$id</td><td>$a</td><td>$_id</td><td>$eq</td></tr>");
                
            }
            
            ?>
            </table>
            Come on now to generate certain random values with variable width<br/>
            <table><tr><td>#</td><td>Random value</td><td>Encode</td><td>Decode</td><td>Ok?</td></tr>
            <?php
            $n = 6;
            // calculates some random values to check if encode grows and decode and runs ok
            for ($i = 0; $i<10; $i++){
                
                $id  = rand(0,pow(90,$i));
                $a   = $alias->encode($id, 1, JaitecAlias::GROWS);
                $_id = $alias->decode($a);
                $eq  = ($id == $_id)?'OK':'Error';
                print("<tr><td>$i</td><td>$id</td><td>$a</td><td>$_id</td><td>$eq</td></tr>");
                
            }
            
            ?>
            </table>
            <?php
        ?>
    </body>
</html>
