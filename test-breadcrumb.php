<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <style type="text/css">
            table{border:1px solid black;}
            td{border:1px solid black;}
        </style>
    </head>
    <body>
        <?php
        
        /**
         * This is a test for JaitecAlias class that demostrates how large
         * can be the id to encode, and how the processes of encode-decode are
         * simetrics
         */
            require 'jaitec/breadcrum/JaitecBreadcrum.php';
            
            $bc = new JaitecBreadcrum();
            
            ?>
            Generating some values<br/>
            <?php 
            for ($n = 0; $n<8; $n++){
                $bc->putLevelAndEraseNexts($n, "level#$n", "#url-for-level-$n");
            }
            
            $bc->paint(true);
            
            print "<br/>HistoryBack = ".JaitecBreadcrum::getHistoryBack()."<br/>";
            
            
            $a = array();
            
            print_r($a);
            
            $a[3]= 100;
            
            print_r($a);
            
            ?>
            
            
    </body>
</html>
