<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        Hello, this is an index file that demonstrates the Jaitec classes:
        <ul>
            
            <li><a href="test-alias.php">Test JaitecAlias class functionality</a></li>
            <li><a href="test-breadcrumb.php">Test JaitecBreadcrumb class funcionality</a></li>
            <li><a href="test-hash.php">Test JaitecHash class funcionality</a></li>
        </ul>
        
        If you enjoy with this, download from <a href="https://bitbucket.org/jlaso/jaitectools">bitbucket</a>
    </body>
</html>

