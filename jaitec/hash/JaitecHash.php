<?php


class JaitecHash
{
    protected $seed = "=abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789%<>/";
    //protected $seed = "=abcdefghijklmnopqrstuvxyz_<>.:,;'ABCDEFGHIJKLMNOPQRSTUWXYZ0123456789%+-&?¿*()/$#@|!";

    public function __construct()
    {
        $n = strlen($this->seed);
        // check non-repeated chars in each array element
        $yet = '';
        for($j=0;$j<$n;$j++){  // $this->n because we determined at previous step all elements have same size
            $c = substr($this->seed, $j, 1);
            if(strpos($yet, $c)) {
                $decored = str_replace($c, " ->$c<- ", $this->seed);
                throw new Exception("Seed has element ($c) repeated, ||$decored||");
            }
            $yet .= $c;
        }

    }
    
    /**
     * encodes the id passed with the number of digits indicated
     * @param integer $id
     * @param integer $digits
     * @return string
     */
    public function encode($id, $digits = 8)
    {
        $seed = $this->seed;
        
        $ret = '';
        $n = strlen($seed);
        $aux = $id;
        
        while($aux>0){
            $h = $aux % $n;
            $ret .= substr($seed,$h,1);
            $aux = ($aux - $h) / $n;            
        }
        $len = strlen($ret);
        if($digits>1){
            $ret = substr($seed,$len,1).$ret;
            $len++;
        }
        if($len<$digits){ 
            $ret .= "=";
            for($i=$len+1;$i<$digits;$i++){
               $ret .= substr($seed,rand(0,$n-1),1);
            }
        }
        return $ret;
    }
    
    public function decode($hash)
    {
        $seed = $this->seed;
        $l = strpos($seed,substr($hash,0,1));
        if(1===strlen($hash)) return $l;
        $id = 0;
        $n = strlen($seed);
        $aux = substr($hash,1,$l);  
        $pot = 1;
        for($i=0;$i<=$l;$i++)
        {
            $h = strpos($seed,substr($aux,$i,1));
            $id = $id + $pot * $h;
            $pot *= $n;
        }
        return $id;
    }
    
    private function bytecode($byte)
    {
        $ret = '';
        $n = strlen($this->seed);
        $d = ceil(256 / $n);
        $aux = $byte;
        while($aux>0){
            $h = $aux % $n;
            $ret .= substr($this->seed,$h,1);
            $aux = ($aux - $h) / $n;            
        }
        for($j=strlen($ret);$j<$d;$j++) 
            $ret .= substr($this->seed,0,1);
        if(strlen($ret)!=$d)            
            throw new Exception("in bytecode($byte) $d differs from ".strlen($ret)." in $ret");
        return $ret;
    }
    
    private function bytedecode($ch)
    {
        $id = 0;
        $n = strlen($this->seed);
        $len = strlen($ch);
        $pot = 1;
        for($i=0;$i<=$len;$i++)
        {
            $h = strpos($this->seed,substr($ch,$i,1));
            $id = $id + $pot * $h;
            $pot *= $n;
        }
        return $id;
    }
    
    public function strToHash($str)
    {
        $len = strlen($str);
        $ret = '';
        for($i=0;$i<$len;$i++)
        {
            $ret .= $this->bytecode(ord(substr($str,$i,1)));
        }
        //print "ret=$ret\r\n";
        return $ret;
    }
    
    public function hashToStr($hash)
    {
        $len = strlen($hash);
        $n = ceil(256 / strlen($this->seed));
        if($len % $n)            
            throw new Exception("for hashToStr('$hash'), error in length, given $len not divisible by $n, length of seed is ".strlen($this->seed));
        $ret = '';
        for($i=0;$i<$len;$i+=$n)
        {
            $ret .= chr($this->bytedecode(substr($hash,$i,$n)));
        }
        return $ret;
    }
    
}

