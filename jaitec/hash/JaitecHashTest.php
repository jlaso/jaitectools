<?php

require dirname(__FILE__)."/JaitecHash.php";

class JaitecHashTest extends PHPUnit_Framework_TestCase
{
    
    public function testRandom()
    {
        
        $hash = new JaitecHash();
        
        for ($i=0;$i<1000;$i++)
        {
            // first get a random number of digits
            $n   = rand(1,15); 
            // now the max id can be represented with this number of digits
            // and a ID betwen 0 and this maxId
            $id  = rand(1,pow(10,($n/2)-1));
            // now encode
            $a   = $hash->encode($id, $n);
            // and decode
            $_id = $hash->decode($a);
            //print "\r\nn=$n\r\nhash generated for $id is: $a, and decode is $_id\r\n";
            // and finally checks that original id and decoded are the same
            $this->assertEquals($id,$_id);
            // check if hash has $n length
            $this->assertEquals($n, strlen($a));
            
        }
        
    }
    
    
    public function testStrHash()
    {
        
        $hash = new JaitecHash();
        
        $strs = array(
            "a",
            "b",
            "aa",
            "This is a test",
            "Second test that is more complicated",
            "first",
            "aName",
            "__12345__abcd__",
            );
        
        foreach($strs as $str)
        {
            $aux  = $hash->strToHash($str);
            // and decode
            $_str = $hash->hashToStr($aux);
            //print "\r\n\r\nhash generated for $str is: $aux, and decode is $_str\r\n";
            //  finally checks that original and decoded are the same
            $this->assertEquals($str,$_str);
            
        }
        
    }
    
    public function testStrHashPower()
    {
        
        $hash = new JaitecHash();
        
        $strs = array(
            "a",
            "b",
            "aa",
            "This is a test",
            "Second test that is more complicated",
            "first",
            "aName",
            "__12345__abcd__",
            );
        
        foreach($strs as $str)
        {
            $aux  = $hash->strToHash($hash->strToHash($str));
            // and decode
            $_str = $hash->hashToStr($hash->hashToStr($aux));
            //print "\r\n\r\nhash generated for $str is: $aux, and decode is $_str\r\n";
            //  finally checks that original and decoded are the same
            $this->assertEquals($str,$_str);
            
        }
        
    }
    
    
}

