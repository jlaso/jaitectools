<?php

/*
 * The functionallity of this class is to have a breadcrum in your project
 * breadcrum is a navigation bar that remembers the origin(home) and the 
 * middle steps to your actual location in the website, and let you access all
 * 
 * Sample to integrate in slim project, add this lines to TwigViewSlim.php:
 * 
 * $twigEnvironment->addFunction('breadcrum',  new Twig_Function_Function('JaitecBreadcrum::getInstance()->paint'));
 * $twigEnvironment->addFunction('historyback',new Twig_Function_Function('JaitecBreadcrum::getInstance()->getHistoryBack'));
 * 
 * @author Joseluis
 * @date 07/06/2012
 */

class JaitecBreadcrumb {
    
    // stores the breadcrum elements
    protected $breadcrumb = array();
    
    // true if the variable session holds same that $breadcrum
    protected $sync = false;
    
    // to get instance for static methods
    protected static $instance = null;
    
    // indicates the level of breadcrum, that is the number of elements
    protected $level = -1;
    
    // indicates if the breadcrum is active and can print when calls paint method
    protected $active = true;
    
    // indicates what is the separator between breadcrum menu options
    protected $separator;
    
    function __construct($active=true,$separator = ' > '){
        $this->separator = $separator;
        if(isset($_SESSION['JaitecBreadcrumb']))
            $this->breadcrumb = $_SESSION['JaitecBreadcrumb'];
        else
            $this->breadcrumb = array();
        $this->sync = true;
        if(self::$instance==null) self::$instance = $this;
        $this->syncLevel();
        $this->active = $active;
    }
    
    /**
     * get the instance has been created or null if not
     * @return JaitecBreadcrum
     */
    public static function getInstance(){
        return self::$instance;        
    } 
    
    /**
     * gets the breadcrum at level passed
     * @param int $level
     * @return array('name','url')
     */
    function getLevel($level){
        return isset($this->breadcrumb[$level])?$this->breadcrumb[$level]:array();
    }
    
    /**
     * gets the element that is in the last position of the breadcrum
     * @return array('name','url') or null
     */
    function getLastLevel(){
        if ($this->level>=1) 
            return $this->getLevel($this->level);
        else 
            return null;
    }
    
    /**
     * gets the element that is in the pre-last position of the breadcrum
     * @return array('name','url') or null
     */
    function getPreLastLevel(){
        if ($this->level>1) 
            return $this->getLevel($this->level-1);
        else 
            return null;
    }
    
    /**
     * sets the $level element of the breadcrum with name and url passed
     * if sync = true  the change is propagated to the varible session 
     * @param int $level
     * @param string $name
     * @param string $url
     * @param boolean $sync 
     */
    function putLevel($level,$name,$url,$sync=true){
        $this->breadcrumb[$level] = array('name'=>$name,'url'=>$url);
        $this->sync = false;
        if($sync) $this->sync();
    }
    
    /**
     * resets the breadcrum and associated variable session
     */
    function reset(){
        $this->breadcrumb = array();
        $this->sync = false;
        $this->sync();
    }
    
    /**
     * erases the n-elements from the level passed to the end of breadcrum
     * if sync = true  the change is propagated to the varible session 
     * @param inc $fromlevel
     * @param boolean $sync 
     */
    function erase($fromlevel=0,$sync=true){
        foreach($this->breadcrumb as $k => $breadcrumb){
            if($k>=$fromlevel)
                unset($this->breadcrumb[$k]);
            $this->sync = false;
        }
        if($sync) $this->sync();
    }
    
    /** 
     * put info from $level and erases nexts levels
     * @param int $level
     * @param string $name
     * @param string $url 
     */
    function putLevelAndEraseNexts($level,$name,$url){
        $this->putLevel($level, $name, $url, false);
        $this->erase($level+1);
        //$this->sync();
    }
    
    /**
     * returns or paints the breadcrum
     * @param boolean $print
     * @return string 
     */
    function paint($print=false,$pretty=true){
        if(!$print && $pretty) return $this->pretty();
        $html = '';
        foreach($this->breadcrumb as $k => $breadcrumb){
            if(isset($breadcrumb['url']))
                $html .= '<a href="'.$breadcrumb['url'].'">'.$breadcrumb['name'].'</a>'
                    .(($k+1<count($this->breadcrumb))?$this->separator:'');
        }
        if($this->active){
            if($print) 
                print $html; 
            else 
                return $html; 
        }else{
            return "";
        }
        
    }
    
    /**
     * returns html for the breadcrum in pretty style
     * @param string $class
     * @return string 
     */
    function pretty($class="JaitecBreadcrumb",$styles="/lib/jaitectools/jaitec/breadcrumb"){
        if($styles){
            $css = file_get_contents(dirname(__FILE__)."/JaitecBreadcrumbSample.css");
            $css = str_replace("<%dir%>", $styles, $css);
            $html = "<style type='text/css'>$css</style>";
        }else{
            $html = '';
        }
        $i = 0;
        foreach($this->breadcrumb as $k => $breadcrumb){
            if(isset($breadcrumb['url'])){
                $int = '<span class="left">&nbsp;</span><span class="center">'.$breadcrumb['name'].'</span><span class="right">&nbsp;</span>';
                $html .= '<span class="'.$class.' ';
                if($k+1<count($this->breadcrumb)){
                    $html .= ($i==0?'first':'no-first').' no-last active"><a href="'.$breadcrumb['url'].'">'.$int.'</a></span>';
                }else{
                    $html .= 'no-first last">'.$int.'</span>';
                }  
            }
            $i++;
        }
        return "<div class='{$class}_div'>$html</div>"; 
    }
    
    /**
     * syncronizes the info in object with variable session
     */
    function sync(){
        if(!$this->sync){
            if(count($this->breadcrumb))
                $_SESSION['JaitecBreadcrumb'] = $this->breadcrumb;
            else
                unset($_SESSION['JaitecBreadcrumb']);
            $this->syncLevel();
            $this->sync = true;
        }
    }
    
    /**
     * actualizes level indicator with real contents of breadcrum
     */
    function syncLevel(){
        $this->level = count($this->breadcrumb)-1;
    }
    
    /**
     * gets the pre-last element at breadcrum, simulates window.history.back() in JS
     * @return string or null 
     */
    public static function getHistoryBack(){
        $bc = self::$instance;
        if($bc){
            $ret = $bc->getPreLastLevel();
            return $ret['url'];
        }
        return null;
    }
}

?>
