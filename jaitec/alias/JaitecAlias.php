<?php

/*
 * The functionallity of this class is to have a method that obtain 
 * pseudo-random id width a seed, normally same id,
 * It generates the base-n id, throught key-cycling process with
 * simulates randomly generated, the advantage of this process is
 * that is reversible and can obtaind primary id with decode
 * principally thinked for aliases in slugs or urls, not for passwords(can decode)
 *
 * the codification is in an inusual format, the first digit is low significant digit.
 * for this reason this code is expandable, most significant digits are added by right
 * 
 * @author Joseluis
 * @date 26/05/2012
 */
class JaitecAlias {
    
    // only one important question: if you code alias for url and you store in
    // database, not change the $base array, because if you decode stored alias
    // do you not obtain original id, understod?
    private $base = array (
        // the purpose of this aparent disorder is to obtain a pseudo-random
        // alias, an alias that can be decoded and obtain original id
        /*00*/'#abc,de0fghij;k9lmn.opq7rs-tuvx5yzABC_DE4FGHIJK3LMN&OP1QRSTUV6XYZ',
        /*01*/'JK3LMN&!tuvx5yzABC_DE4FGHIOP1QRSTUV6XYZabc,de0fghij;k9lmn.opq7rs-',
        /*02*/'STUV6XYZabc,de0fghij;k9lmK3LMNn.opq7rs-!tuv&OP1QRx5yzABC_DE4FGHIJ',
        /*03*/'OP1QR;k9lmn.opq7rsSTdzABC_DEuJK3LMN4FGHe0fghij!t-&vx5yUV6XYZabc,I',
        /*04*/'lmn.op_DE4FGHe0fghij!tuJK3LMNvx5q7rsSTdzABC,I-&OPyUV6XYZabc1QR;k9',
        /*05*/'vx5yzABC_DE4FGHUV6XYZ;k9lmn.opq7rs-&OP1QRSTde0fghij!tuabc,IJK3LMN',
        /*06*/'TdzABC,I-&OPyUV6XYZabc1QR;k9lmn.op_DE4FGHe0fghij!tuJK3LMNvx5q7rsS',
        /*07*/'QRSTde0fghij!tuabc,IJK3LMNvx5yzABC_DE4FGHUV6XYZ;k9lmn.opq7rs-&OP1',
        /*08*/'4FGHe0fghij!t-&OP1QR;k9lmn.opq7rsSTdzABC_DEuJK3LMNvx5yUV6XYZabc,I',
        /*09*/'q7rsSTdzABC,I-&OPyUV6XYZabc1QR;k9lmn.op_DE4FGHe0fghij!tuJK3LMNvx5',
        // can you continue or not, because is cycled in code, for digit 11th
        // it starts at 0
    );
    
    // this holds base-n of this class, calculated at run-time
    private $n = 0;
    
    // this holds number of cycling bases for each n-digit
    private $l = 0; 
    
    // convention for last parameter encode function
    const GROWS = true;
    
    // large integer can be represented
    const MAX = 999999999999999999; //21474836470;
    
    // number max digits can need to represent MAX
    protected $maxbit = null;
    
    /**
     * Checks if the base array is ok, and if not throws exception indicating this
     */
    function __construct() {
        $this->l = count($this->base);
        // check now if array base is correctly formed, i.e. same lenght for
        // all elements and  non-repeated chars
        for($i=0;$i<$this->l;$i++){ 
            $aux = strlen($this->base[$i]);
            if($this->n){  // now check same sized elements
                if($aux<>$this->n)
                    throw new Exception("Not same sized for element 0 and ".$i);
            }else
                $this->n = $aux;
        }
        // now check non-repeated chars in each array element
        for($i=0;$i<$this->l;$i++){ 
            $yet = '';
        
            for($j=0;$j<$this->n;$j++){  // $this->n because we determined at previous step all elements have same size
                $c = substr($this->base[$i], $j, 1);
                if(strpos($yet, $c)) {
                    $decored = str_replace($c, " ->$c<- ", $this->base[$i]);
                    throw new Exception("Element $i has element ($c) repeated, ||$decored||");
                }
                $yet .= $c;
            }
        }
        $this->maxbit = strlen($this->encode(self::MAX,1,true));
    }
    
    /**
     * encodes numeric id in n-based version, with almost maxbit digits and more if grows true
     * @param integer $id
     * @param byte $maxbit
     * @param boolean $grows
     * @return string 
     */
    public function encode($id, $maxbit = 8, $grows = false){
        if($id>self::MAX)
            throw new InvalidArgumentException("Maximum id is ".self::MAX." in JaitecAlias->encode, {$id} given");
        if(null!==$this->maxbit && $maxbit>$this->maxbit)
            throw new InvalidArgumentException ("Second argument for JaitecAlias->encode must less than {$this->maxbit}, {$maxbit} given");
        if($id<0)
            throw new InvalidArgumentException ("First argument for JaitecAlias->encode must be positive integer, {$id} given");
        if(!$grows && $id>$this->maxId($maxbit))
            throw new InvalidArgumentException("Limit id($id) for $maxbit digits in JaitecAlias->encode, possible lost of information");
        $ret = '';
        $aux = $id;
        $bit = 0;
        if($maxbit<=0) $maxbit = 1;
        //if(0===$id) return substr($this->base[0],0,1);
        //if(1===$id) return substr($this->base[0],1,1);
        while(($grows && $aux) || $maxbit>$bit){
            $a = $aux % $this->n;
            $ret .= substr($this->base[$bit % $this->l],$a,1);
            $aux = ($aux-$a)/$this->n;
            $bit++;
            //if($aux<=0) break;
        }
        return $ret;
    }
    
    /**
     * decodes an alias obtained previously with encode and returns the id that originates this alias
     * @param strig $alias
     * @return integer 
     */
    public function decode($alias){
        $id = 0;
        $n = strlen($alias);
        $x = 1;
        for($bit=0;$bit<$n;$bit++){
            $c = substr($alias,$bit,1);
            $v = strpos($this->base[$bit % $this->l],$c);
            $id += $v * $x;
            $x *= $this->n;
        }
        
        return $id;
    }

    /**
     * calculats the last alias can be formed with n-digits
     * @param integer $digits
     * @return string 
     */
    public function maxAlias($digits=8){
        $alias = '';
        for ($bit=0;$bit<$digits;$bit++){
            $alias .= substr($this->base[$bit % $this->l],-1);
        }
        return $alias;
    }
    
    /**
     * calculates the id that applies for the last alias can be formed
     * @param integer $digits
     * @return integer
     */
    public function maxId($digits=8){
        $aux = pow($this->n, $digits)-1;
        return ($aux>self::MAX)?self::MAX:$aux;
    }
    
    
    public function getMaxbit(){
        return $this->maxbit;
    }
}

?>
