<?php

require dirname(__FILE__)."/JaitecAlias.php";

class JaitecAliasTest extends PHPUnit_Framework_TestCase
{
    /**
     * @expectedException InvalidArgumentException
     */
    public function testNegativeArgument()
    {
        //print "\r\n\r\n|".(999999999999999999)."|\r\n\r\n";
        $this->setExpectedException('InvalidArgumentException');
        $alias = new JaitecAlias();
        $arg = -12345;
        $void = $alias->encode($arg);
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testExcededArgument()
    {
        $this->setExpectedException('InvalidArgumentException');
        $alias = new JaitecAlias();
        $max = $alias->maxId(8);
        $arg = $max+1;
        $void = $alias->encode($arg,8,false);
    }
     
    /**
     * @expectedException InvalidArgumentException
     */
    public function testExcededMaxbitArgument()
    {
        $this->setExpectedException('InvalidArgumentException');
        $alias = new JaitecAlias();
        $arg = 12345;
        $void = $alias->encode($arg,12);
    }
    
    public function testFixedNumber_MAX()
    {
        $alias = new JaitecAlias();
        
        $this->assertEquals(JaitecAlias::MAX,$alias->decode($alias->encode(JaitecAlias::MAX,1,true)));
        
    }
    
    public function testFixedNumber_0_65()
    {
        
        $alias = new JaitecAlias();
        
        $o = 1;   $this->assertEquals($o,$alias->decode($alias->encode($o,1,true)));
        $o = 65;  $this->assertEquals($o,$alias->decode($alias->encode($o,1,true)));
        
    }
    
    public function testRandom()
    {
        
        $alias = new JaitecAlias();
        $nmax  = $alias->getMaxbit();
        
        for ($i=0;$i<10000;$i++)
        {
            // first get a random number of digits
            $n   = rand(1,$nmax);
            // now the max id can be represented with this number of digits
            // and a ID betwen 0 and this maxId
            $id  = rand(0,$alias->maxId($n));
            // now encode
            $a   = $alias->encode($id, $n);
            // and decode
            $_id = $alias->decode($a);
            // and finally checks that original id and decoded are the same
            $this->assertEquals($id,$_id);
        }
        
    }
    
    public function testRandomWithGrows()
    {
        
        $alias = new JaitecAlias();
        
        for ($i=0;$i<10000;$i++)
        {
            $n   = rand(1,$alias->getMaxbit()-1);
            // the id must be represented with plus than $n digits
            $id  = $alias->maxId($n) + rand(1,10000);
            // now encode                   v-- grow flag
            $a   = $alias->encode($id, 0, true);
            // and decode
            $_id = $alias->decode($a);
            // and finally checks that original id and decoded are the same
            $this->assertEquals($id,$_id);
        }
              
    }
    
    /**
     * @expectedException InvalidArgumentException
     */
    public function testRange()
    {
        
        $alias = new JaitecAlias();
        
        for ($i=0;$i<5;$i++)
        {
            // first get a random number of digits
            $n   = rand(0,10)+1;
            // now the max id can be represented with this number of digits
            // and a value betwen 0 and this maxId
            $val = rand(0,$alias->maxId($n));
            // and now test a continuos range between -12345 and 12345 from $val
            for($id=$val-12345;$id<=$val+12345;$id++){
                // now encode
                $a   = $alias->encode($id, $n);
                // and decode
                $_id = $alias->decode($a);
                // and finally checks that original id and decoded are the same
                if($id>=0)
                    $this->assertEquals($id,$_id);
                
            }
        }
        
    }
    
    
}

